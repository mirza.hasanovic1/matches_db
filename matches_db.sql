CREATE DATABASE MATCHES_DB;

CREATE TABLE TEAM (
    team_id INTEGER NOT NULL PRIMARY KEY,
    name text
);

CREATE TABLE MATCHES (
    match_id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    start_time timestamp,
  	home_team_id INTEGER,
    guest_team_id INTEGER,
  	home_team_score INTEGER,
  	guest_team_score INTEGER
);

-- insert
INSERT INTO TEAM VALUES (0001, 'Chelsea');
INSERT INTO TEAM VALUES (0002, 'Liverpool');
INSERT INTO TEAM VALUES (0003, 'Real Madrid');
INSERT INTO TEAM VALUES (0004, 'Barcelona');
INSERT INTO MATCHES VALUES (0001, CURRENT_TIMESTAMP(), 0001, 0002, 1, 0);
INSERT INTO MATCHES VALUES (0002, CURRENT_TIMESTAMP(), 0003, 0004, 1, 2);

-- fetch all matches
SELECT * FROM MATCHES;

-- fetch all matches of Barcelona
SELECT * FROM MATCHES INNER JOIN TEAM on MATCHES.home_team_id = TEAM.team_id 
where name = 'Barcelona';

-- fetch all matches with at least one goal
SELECT * FROM MATCHES where (home_team_score <> 0 or guest_team_score <> 0);